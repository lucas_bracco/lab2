from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic
from PyQt5 import QtCore


class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("2020-10-29/02-lcd.ui", self)
        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.mostrarHora)
        timer.start(1000)
        self.mostrarHora()

    def mostrarHora(self):
        horaActual = QtCore.QTime.currentTime()
        horaTexto = horaActual.toString('hh:mm:ss')
        self.hora.display(horaTexto)


app = QApplication([])

win = MiVentana()
win.show()

app.exec_()
