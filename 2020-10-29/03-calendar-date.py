from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic


class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("2020-10-29/03-calendar-date.ui", self)
        self.calendario.selectionChanged.connect(self.on_cambiar_fecha)
        self.on_cambiar_fecha()

    def on_cambiar_fecha(self):
        # QDate
        self.fecha.setDate(self.calendario.selectedDate())


app = QApplication([])

win = MiVentana()
win.show()

app.exec_()
