#Variables:
numero_entero = 10
numero_decimal = 10.5
cadena1 = 'Un Texto'
cadena2 = "Otro Texto"
booleano = True
#Diferentes formas de mostrar texto por pantalla:
print("Hola Mundo!")
print(cadena1,cadena2,numero_entero,booleano)
print("El número vale: {0} - {1} - {0}".format(numero_entero,numero_decimal))
print(f"El número vale: {numero_entero} - {numero_decimal} - {numero_entero}")