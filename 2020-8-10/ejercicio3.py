#Pedir valor por teclado:
nombre = input("Ingrese nombre: ")#El valor ingresado por input devuelve siempre una cadena de caracteres.
print(nombre)
num1 = int(input("Ingrese número 1: "))#Convierte la cadena ingresada por teclado en entero.
num2 = float(input("Ingrese número 2: "))#Convierte la cadena ingresada por teclado en flotante.
print(num1 + num2)
#Otra forma de hacerlo es:
num3 = input("Ingrese número 3: ")
num4 = input("Ingrese número 4: ")
print(int(num3) + float(num4))