from functools import reduce

numeros = [-4, 5, 2, 6, 2, 7, 0, 12, 5, 9]

def fun_suma(a, b):
    #print("a =", a)
    #print("b =", b)
    return a + b

suma = reduce(fun_suma,numeros)

print(suma)