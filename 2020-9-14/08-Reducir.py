from functools import reduce

numeros = [-4, 5, 2, 6, 2, 7, 12, 5, 9]

# Empleando 'reducir' encontrar el producto de cada elemento de la lista

def fun_mult(a, b):
    return a * b

mult = reduce(fun_mult,numeros)

print(mult)