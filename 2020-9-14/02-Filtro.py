edades = [13,23,12,18,27,35,67,8,4,23,44,51,13,32,14]

def mayor(edad):
    return edad >= 18

#def menor(edad):
#    return edad < 18

#filter es una función que devuelve un objeto.
mayores = list(filter(mayor,edades))
menores = list(filter(lambda edad: edad < 18,edades))
print(mayores)
print(menores)