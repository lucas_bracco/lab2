edades = [13,23,12,18,27,35,67,8,4,23,44,51,13,32,14]

def par(edad):
    return edad % 2 == 0

pares = list(filter(par,edades))
impares = list(filter(lambda edad: edad % 2 == 1,edades))

print(pares)
print(impares)