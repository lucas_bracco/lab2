from math import pi

angulos = [90, 180, 270, 360, 45]

# Utilizar 'map' y generar una nueva lista en radianes.

def rad(ang):
    return ang * pi / 180

radianes = list(map(rad,angulos))

print(angulos)
print(radianes)