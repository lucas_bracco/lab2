#Pedir al usuario ancho y alto del rectángulo.
#Crear objeto rectángulo.
#Implementar metodo: perímetro y área.

from rectangulo import Rectangulo

alt = float(input("Ingrese altura del rectángulo: "))
lar = float(input("Ingrese largo del rectángulo: "))

rectangulo = Rectangulo(alt,lar)

print("El área del rectángulo es:", round(rectangulo.area(),2))
print("El perímetro del rectángulo es:", round(rectangulo.perimetro(),2))