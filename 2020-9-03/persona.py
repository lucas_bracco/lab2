import random

class Persona:

    def __init__(self, nombre, apellido, edad, dni):
        self.nombre = nombre
        self.apellido = apellido
        self.edad = edad
        self.dni = dni
        print("Objeto persona creado")
    
    def nombre_completo(self):
        return self.apellido + ", " + self.nombre
    
    #El metodo __str__ es llamarlo cuando usamos print(*nombre del objeto*)
    def __str__(self):
        return '{0}, {1}, {2}, {3}'.format(self.apellido, self.nombre, self.edad, self.dni)

def Nombre_RNG ():
    palabra = ""
    for i in range(8):
        letra = chr(random.randint(ord('a'),ord('z')))
        if i % 2 == 0:
            while letra in ("a","e","i","o","u"):
                letra = chr(random.randint(ord('a'),ord('z')))
            palabra += letra
        if i % 2 == 1:
            while not (letra in ("a","e","i","o","u")):
                letra = chr(random.randint(ord('a'),ord('z')))
            palabra += letra
    return palabra.title()