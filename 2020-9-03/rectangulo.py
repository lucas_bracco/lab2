class Rectangulo:
    def __init__(self,alto,largo):
        self.alto = alto
        self.largo = largo
        print("Objeto rectángulo creado con altura:", self.alto,"y largo:",self.largo)
    
    def area(self):
        return self.alto * self.largo

    def perimetro(self):
        return 2 * (self.alto + self.largo)