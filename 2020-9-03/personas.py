#Crear una Clase personas
#Parámetros: Lista_personas
#Métodos:
#   Agregar: Agrega un objeto persona a la lista.
#   Mayor: Devuelve la persona mayor.
#   Menor: Devuelve la persona menor.

class LdPersonas:
    
    def __init__(self, persona):
        self.persona = persona