from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5 import uic

class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("2020-10-08/03-radio.ui",self)
        self.boton_1.clicked.connect(self.on_clicked)

    def on_clicked(self):
        if self.rboton_1.isChecked():
            x = "1"
        elif self.rboton_2.isChecked():
            x = "2"
        elif self.rboton_3.isChecked():
            x = "3"
        if self.rboton_a.isChecked():
            y = "a"
        elif self.rboton_b.isChecked():
            y = "b"
        elif self.rboton_c.isChecked():
            y = "c"
        self.etiqueta.setText(f"Se eligió la opción {x} y la opción {y}")
app = QApplication([])

win = MiVentana()
win.show()

app.exec_()