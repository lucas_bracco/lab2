from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5 import uic

class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("2020-10-08/04-check.ui",self)
        self.boton_1.clicked.connect(self.on_clicked)

    def on_clicked(self):
        x = 0
        if self.cb_1.isChecked():
            x += 20
        if self.cb_2.isChecked():
            x += 50
        if self.cb_3.isChecked():
            x += 70
        self.etiqueta.setText(f"Precio total agregado: ${x}")
app = QApplication([])

win = MiVentana()
win.show()

app.exec_()