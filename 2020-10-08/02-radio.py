from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5 import uic

class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("2020-10-08/01-radio.ui",self)
        self.rboton_1.toggled.connect(self.on_clicked)
        self.rboton_2.toggled.connect(self.on_clicked)
        self.rboton_3.toggled.connect(self.on_clicked)
    
    def on_clicked(self):
        print("Cambio de Estado.")
        if self.rboton_1.isChecked():
            self.etiqueta.setText("Se elige la opción 1")
        elif self.rboton_2.isChecked():
            self.etiqueta.setText("Se elige la opción 2")
        elif self.rboton_3.isChecked():
            self.etiqueta.setText("Se elige la opción 3")
        else:
            self.etiqueta.setText("No se eligió ninguna opción")

app = QApplication([])

win = MiVentana()
win.show()

app.exec_()