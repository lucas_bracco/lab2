dic = {
    "Fruta1" : "Naranja",
    "Fruta2" : "Manzana",
    "Fruta3" : "Durazno",
    5 : "Un número"
}
#Imprime el diccionario completo
print(dic)
#Imprime el contenido del elemento seleccionado del diccionario.
print(dic["Fruta2"])
print(dic[5])
#Devuelve los elementos contenidos en el diccionario.
print(dic.keys())
#Devuelve los valores que hay en el diccionario.
print(dic.values())
#Elimina el elemento seleccionado.
dic.pop("Fruta2")
print(dic)
#Para modificar un elemento del diccionaro se hace de la siguiente forma:
dic["Fruta1"] = "Mandarina"
print(dic)