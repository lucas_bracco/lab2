#Cadenas Simples.
cadena1 = 'Texto en "comilla" simple.'
cadena2 = "Texto en 'comilla' doble."
#Cadena de varias lineas.
cadena3 = """Esta es 
una cadena
de varias lineas"""

print(cadena1)
print(cadena2)
print(cadena3)