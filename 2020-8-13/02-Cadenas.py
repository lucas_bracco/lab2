cadena = "Esto es un texto."

print(type(cadena))
print(dir(cadena))
print(cadena.upper())
print(cadena.lower())
print(cadena.index("es"))#Indica a partir de que elemento empieza "es"
print(cadena.count("e"))#Cuenta la cantidad de "e" que contiene el texto o la palabra.
print(len(cadena))#Cuenta la cantidad de letras que contiene el texto o la palabra.
print(cadena[-1])