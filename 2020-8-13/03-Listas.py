lista = [0, 4, 6, 5, -78, 10, 5, 3]
#Imprimir la lista completa.
print(lista)
#Imprimir un elemento de la lista.
print(lista[5])
#Imprimir elementos de la lista desde el elemento 1 hasta el elemento 4.
print(lista[1:5])
#Imprime desde el elemento 1 hasta el final.
print(lista[1:])
#Imprime desde el primer elemento hasta el elemento 5.
print(lista[:6])
#Imprime de tras para delante.
print(lista[::-1])
lista1 = [1,2,3]
lista2 = [4,5]
#Concatenación de Listas
lista3 = lista1 + lista2
print(lista3)