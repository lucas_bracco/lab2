lista = [-1, 3, 11]
print(lista)
#Cuenta la cantidad de elementos dentro de una lista.
print(len(lista))
#Agrega un nuevo elemento al final de la lista.
lista.append(-78)
print(lista)
#Concatena nuevos elementos al final de la lista.
lista.extend([2,-10])
print(lista)
#Inserta un elemento nuevo (5) sin reemplazar en la posicion indicada (2).
lista.insert(2,5)
print(lista)
#Ordena la lista de forma invertida.
lista.reverse()
print(lista)
#Ordena la lista de manera ascendente.
lista.sort()
print(lista)
#Ordena la lista de manera descendente.
lista.sort(reverse=True)
print(lista)
#Elimina el elemento en el indice indicado (3).
lista.pop(3)
print(lista)
#Elimina el elemento especificado (si hay dos elementos iguales, elimina el primero que se encuentra).
lista.remove(-78)
print(lista)
#Pregunta si el elemento indicado se encuentra en la lista (Devuelve True o False).
print(5 in lista)