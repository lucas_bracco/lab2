from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic


class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("2020-10-26/03-progress_bar.ui", self)
        self.progreso.valueChanged.connect(self.on_progreso)
        self.descargar.clicked.connect(self.on_descargar)

    def on_progreso(self):
        self.etiqueta.setText(str(self.progreso.value()))

    def on_descargar(self):
        descarga = 0.0
        while descarga < 100.0:
            descarga += 0.00001
            self.progreso.setValue(int(descarga))


app = QApplication([])

win = MiVentana()
win.show()

app.exec_()
