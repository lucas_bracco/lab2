from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout,QHBoxLayout, QLabel, QWidget

# Crear Aplicación.
app = QApplication([])

# Crear ventana principal.
ventana = QMainWindow()
ventana.setWindowTitle('Primer Programa')
ventana.setMinimumSize(500,250)

# Crear Layout
widget = QWidget()
layout = QVBoxLayout()
label1 = QLabel("Hola Mundo!")
label2 = QLabel("Chau Mundo!")

layout.addWidget(label1)
layout.addWidget(label2)
widget.setLayout(layout)
ventana.setCentralWidget(widget)
ventana.show()

# Ejecutar una Aplicación.
app.exec_()