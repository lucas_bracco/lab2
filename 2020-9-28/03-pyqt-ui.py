from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5 import uic

class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("2020-9-28/ventana.ui",self)
        #uic.loadUi("2020-9-28/ejemplo.ui",self)

app = QApplication([])

win = MiVentana()
win.show()

app.exec_()