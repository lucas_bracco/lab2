from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton
from PyQt5 import uic
import sqlite3

conexion = sqlite3.connect("2020-11-19/Agenda.db")

cursor = conexion.cursor()


class vCarga(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("2020-11-19/vCargar.ui",self)
        self.indice = 0
        self.lista1 = []
        self.b_Primero.setEnabled(False)
        self.b_Anterior.setEnabled(False)
        self.b_Siguiente.setEnabled(False)
        self.b_Ultimo.setEnabled(False)
        self.b_Cargar.clicked.connect(self.clicked_cargar)
        self.b_Primero.clicked.connect(self.clicked_primero)
        self.b_Anterior.clicked.connect(self.clicked_anterior)
        self.b_Siguiente.clicked.connect(self.clicked_siguiente)
        self.b_Ultimo.clicked.connect(self.clicked_ultimo)
    
    def clicked_cargar(self):
        if self.le_Id.text() == '':
            cursor.execute(""" SELECT * FROM Contactos""") #Selecciona todos los datos de la Tabla Contactos.
            self.lista1 = cursor.fetchall() #Se genera una lista de tuplas.
            self.mostrar()
        else:
            print("Ya hay datos cargados.")
    
    def clicked_primero(self):
        self.indice = 0
        self.mostrar()
    
    def clicked_anterior(self):
        self.indice -= 1
        self.mostrar()
    
    def clicked_siguiente(self):
        self.indice +=1
        self.mostrar()
    
    def clicked_ultimo(self):
        self.indice = len(self.lista1) - 1
        self.mostrar()
    
    def mostrar(self):
        if self.indice == 0:
            self.b_Primero.setEnabled(False)
            self.b_Anterior.setEnabled(False)
            self.b_Siguiente.setEnabled(True)
            self.b_Ultimo.setEnabled(True)

        elif self.indice == len(self.lista1) - 1:
            self.b_Primero.setEnabled(True)
            self.b_Anterior.setEnabled(True)
            self.b_Siguiente.setEnabled(False)
            self.b_Ultimo.setEnabled(False)
            
        else:
            self.b_Primero.setEnabled(True)
            self.b_Anterior.setEnabled(True)
            self.b_Siguiente.setEnabled(True)
            self.b_Ultimo.setEnabled(True)

        self.le_Id.setText(str(self.lista1[self.indice][0]))
        self.le_Nombre.setText(str(self.lista1[self.indice][1]))
        self.le_Completo.setText(str(self.lista1[self.indice][2]))
        self.le_Email.setText(str(self.lista1[self.indice][3]))
#-----------------------------------------------------------------------



app = QApplication([])

ventana = vCarga()
ventana.show()

app.exec_()

conexion.commit()
conexion.close()