class User:
    def __init__(self, username, name, lastname, email, password):
        self.username = username
        self.name = name
        self.lastname = lastname
        self.email = email
        self.password = password           

    def __str__(self):
        return f'''Nombre de usuario:{self.username}
Nombre:{self.name}
Apellido:{self.lastname}
Email:{self.email}        
        '''

class Users:
    def __init__(self):
        self.users = []

    #Pide las campos necesarios para instanciar un User
    def createUser(self):
        print('Ingrese su nombre de usuario:')
        username = input()
        print('Ingrese su nombre:')
        name = input()
        print('Ingrese su apellido:')
        lastname = input()
        print('Ingrese su email:')
        email = input()
        print('Ingrese su contraseña:')
        password = input()
        print('Repita su contraseña:')
        repassword = input()
        if self.validateUser(username, name, lastname, email, password, repassword):
            newUser = User(username, name, lastname, email, password)
            self.users.append(newUser)

    #Corrobora que los campos esten llenos y las pw coincidan
    def validateUser(self, username, name, lastname, email, password, repassword):
        if (username.strip() == '' or name.strip() == '' or
        email.strip() == '' or password.strip() == '' or repassword.strip() == ''):
            print('Ningun campo puede estar vacio')
            return False
        
        if password != repassword:
            print('Las contraseñas no coinciden')
            return False 

        if self.findByUsername(username) != -1:
            print('El usuario ya existe')
            return False       
        return True

    #Devuelve un usuario por nombre de usuario
    def findByUsername(self, username):
        result = list(filter(lambda user: user.username.lower() == username.lower() , self.users))
        if len(result) == 0:
            return -1
        return result[0]

    #lista los usuarios
    def listUsers(self):
        for user in self.users:
            print(user)
