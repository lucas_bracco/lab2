from Group import Group, Groups
from User import User, Users

users = Users()
groups = Groups()

def iniciarSesion():
    print('Ingrese su nombre de usuario')
    username = input()
    print('Ingrese su contraseña')
    pw = input()
    usuario = users.findByUsername(username)
    if usuario == -1:
        print('El usuario no existe')
        return False
    if not usuario.password == pw:
        print('Contraseña inccorecta')
        return False 
    print('Acceso correcto')

def buscarUsuario():
    print('Ingresa el usuario a buscar')
    username = input()
    usuario = users.findByUsername(username)
    if usuario != -1:
        print(usuario)
    else:
        print('El usuario no existe')


def menuChoice():
    print('''
    1-Iniciar Sesion
    2-Registrarse
    3-Buscar un usuario
    4-Crear un grupo
    5-Agregar usuario a un grupo
    6-Eliminar usuario de un grupo 
    7-Listar Usuarios
    8-Listar Grupos   
    9-Salir''')
    return input()

def menu():
    while True:
        option = int(menuChoice())
        if option == 1:            
            iniciarSesion()
            
        elif option == 2:
            users.createUser()

        elif option == 3:
            buscarUsuario()
        elif option == 4:
            groups.createGroup()

        elif option == 5:
            print('Ingrese el nombre del grupo')
            nombreGrupo = input()
            grupo = groups.findByName(nombreGrupo)            
            if grupo == -1:
                print('Grupo no encontrado')
                continue
            print('Ingrese el nombre del usuario')
            nombreUsuario = input()
            usuario = users.findByUsername(nombreUsuario)
            if usuario == -1:
                print('Usuario no encontrado')
                continue
            grupo.addUser(usuario)

        elif option == 6:
            print('Ingrese el nombre del grupo')
            nombreGrupo = input()
            grupo = groups.findByName(nombreGrupo)            
            if grupo == -1:
                print('Grupo no encontrado')
                continue
            print('Ingrese el nombre del usuario')
            nombreUsuario = input()
            usuario = users.findByUsername(nombreUsuario)
            if usuario == -1:
                print('Usuario no encontrado')
                continue
            grupo.deleteUser(usuario)

        elif option == 7:
            users.listUsers()
        elif option == 8:
            groups.listGruops()
        elif option == 9:
            break         

menu()
