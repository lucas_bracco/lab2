from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic


class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("2020-11-02/02-menu.ui", self)
        self.actionNuevo.triggered.connect(self.on_nuevo)
        self.actionAbrir.triggered.connect(self.on_abrir)
        self.actionGuardar.triggered.connect(self.on_guardar)
        self.actionSalir.triggered.connect(self.on_salir)

    def on_nuevo(self):
        print("nuevo")

    def on_abrir(self):
        print("abrir")

    def on_guardar(self):
        print("guardar")

    def on_salir(self):
        app.quit()


app = QApplication([])

win = MiVentana()
win.show()

app.exec_()
