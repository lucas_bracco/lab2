#Como gran lector necesito un software que me permita ir registrando los libros que voy leyendo y contabilizar las 
# horas de lectura que hice con cada uno. Para ello necesito poder :

#Cargar libros con sus datos : Nombre, Año, Editorial, Autor , Cantidad de Páginas.
#Cargar géneros para englobar varios libros similares, lo único que debería ingresar sería : Nombre del Género
#Permitirme agregar los libros a un género específico
#Cargar las horas de lectura de cada uno de esos libros.
#Listar de mayor a menor cuáles fueron los géneros que más demoré en leer y los que menos.

class Libros:
    def __init__(self,nombre,año,edit,autor,cant_pag):
        self.nombre = nombre
        self.año = año
        self.edit = edit
        self.autor = autor
        self.cant_pag = cant_pag
        self.horas = 0

    def __str__(self):
        return f'''Nombre del libro: {self.nombre}
Año: {self.año}
Editorial: {self.edit}
Autor: {self.autor}
Cantidad de Páginas: {self.cant_pag}'''

    def cargar_horas(self,numero):
        self.horas = numero

class Generos:
    def __init__(self,nomb_gen):
        self.nomb_gen = nomb_gen
        self.listalibros = []
        self.total = 0

    def __str__(self):
        return f'Género:{self.nomb_gen}'

    def agregar_libro(self,book):
        self.listalibros.append(book)
        print("Se agregó", book)

    def listado(self):
        print("Género: ",self.nomb_gen,"\nListado de Libros: ")
        for item in self.listalibros:
            print(item.nombre)
    def total_tiempo(self):
        for item in self.listalibros:
            self.total += item.horas
                    
#-----------------------------------------------------------

list_libros = []
list_generos = []
while True:
    print("""Elija una Opción:
    1-Cargar Libro.
    2-Cargar Género.
    3-Agregar Libro a un Genero.
    4-Cargar horas de libros.
    5-Listar generos por tiempo.
    6-Ver lista de Libros.
    7-Ver lista de Géneros.
    0-Salir.""")
    op = int(input())
    if op == 1:
        nomb = input("Ingrese nombre del libro: ")
        año = input("Ingrese año del libro: ")
        edit = input("Ingrese la editorial del libro: ")
        autor = input("Ingrese el autor del libro: ")
        paginas = int(input("Ingrese la cantidad de páginas del libro: "))
        libro = Libros(nomb,año,edit,autor,paginas)
        list_libros.append(libro)
        print("Libro agregado.")
    elif op == 2:
        nomb = input("Ingrese el nombre del genero: ")
        genero = Generos(nomb)
        list_generos.append(genero)
        print("Género agregado.")
    elif op == 3:
        i = 0
        for item in list_libros:            
            print(i,"-",item.nombre)
            i += 1
        x = int(input("Seleccione que libro quiere agregar: "))
        i = 0
        for item in list_generos:
            print(i,"-",item.nomb_gen)
            i += 1
        y = int(input("Seleccione el genero al que quiere agregar el libro: "))
        list_generos[y].agregar_libro(list_libros[x])
        
    elif op == 4:
        i = 0
        for item in list_libros:            
            print(i,"-",item.nombre)
            i += 1
        x = int(input("Seleccione el libro que quiere agregar horas: "))
        n = float(input("Agregue las horas de lectura: "))
        list_libros[x].cargar_horas(n)
    elif op == 5:
        for item in list_generos:
            item.total_tiempo()
        for item in list_generos:
            print("Genero:",item.nomb_gen,"\nCantidad de Horas:",item.total)
        print
    elif op == 6:
        for item in list_libros:
            print(item)
    elif op == 7:
        for item in list_generos:
            item.listado()
    elif op == 0:
        break