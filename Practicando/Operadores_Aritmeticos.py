#Operadores Aritméticos
num1 = 10
num2 = 3
#Suma
print("Suma:",num1,"+",num2,"=",num1 + num2)
#Resta
print("Resta:",num1,"-",num2,"=",num1 - num2)
#Multiplicación
print("Multiplicación:",num1,"*",num2,"=",num1 * num2)
#División
print("División:",num1,"/",num2,"=",num1 / num2)
#División Entera
print("División Entera:",num1,"//",num2,"=",num1 // num2)
#Modulo
print("Modulo:",num1,"%",num2,"=",num1 % num2)
#Potencia
print("Potencia:",num1,"**",num2,"=",num1 ** num2)