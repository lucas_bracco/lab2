#Type es una función que nos permite identificar el tipo de la variable ingresada.

entero = 5
flotante = 5.5
booleana = True
texto = "Texto"

print("-Valor:",entero,"-Tipo:",type(entero))
print("-Valor:",flotante,"-Tipo:",type(flotante))
print("-Valor:",booleana,"-Tipo:",type(booleana))
print("-Valor:",texto,"-Tipo:",type(texto))

#Una misma variable puede tomar distintos tipos dependiendo el dato que guarda.
variable = 1
print("-Valor:",variable,"-Tipo:",type(variable))
variable = 1.5
print("-Valor:",variable,"-Tipo:",type(variable))
variable = False
print("-Valor:",variable,"-Tipo:",type(variable))
variable = "Hola Mundo"
print("-Valor:",variable,"-Tipo:",type(variable))
