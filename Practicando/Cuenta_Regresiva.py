from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout,QHBoxLayout, QLabel, QWidget
from PyQt5 import QtCore
from PyQt5 import uic



class vTime(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("Practicando/Cuenta_Regresiva.ui",self)
        self.i = 0
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.funcion)
        self.timer.start(1000)

    def funcion(self):        
        if self.i == 11:
            self.timer.stop()
            self.i = 0
        aux = 10 - self.i
        self.i += 1
        self.lcd_Timer.display(aux)        

app = QApplication([])

ventana_time = vTime()

ventana_time.show()

app.exec_()
            