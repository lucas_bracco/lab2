import sqlite3

conexion = sqlite3.connect('Practicando/probando.db')

cursor = conexion.cursor()


#FORMA DIRECTA DE CARGAR EN LA BASE DE DATOS
#cursor.execute("""INSERT INTO Probando(Nombre,Continente) VALUES("Francia","Europa")""")

#CARGAR EN LA BASE DE DATOS CON VARIABLES:
lista1 = []
cursor.execute("""SELECT Nombre FROM Probando""")
for item in cursor:
    lista1.append(item[0])

nomb = input("Ingrese nombre del país: ")
cont = input("Ingrese nombre del continente: ")
if nomb in lista1:
    print("El país ya fue ingresado")
else:
    if (len(cont) > 0) and (len(nomb) > 0):
        cursor.execute("INSERT INTO Probando(Nombre,Continente) VALUES(?,?)",(nomb,cont))
        print("País ingresado correctamente.")
    else:
        print("No puede ingresar campos vacios.")

cursor.execute(""" SELECT Nombre FROM Probando WHERE Continente == "Europa" """)
for item in cursor:
    print(item[0])



conexion.commit()

conexion.close()