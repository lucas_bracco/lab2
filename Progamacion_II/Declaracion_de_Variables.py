#Declaración de variables
letra = 'a'
texto1 = "Hola"
texto2 = "Mundo"
entero = 10
decimal = 10.5

#Impresión de variables
print(letra)
print(type(letra))
print(texto1)
print(type(texto1))
print(texto2)
print(type(texto2))
print(entero)
print(type(entero))
print(decimal)
print(type(decimal))

#Concatenación y suma de variables
print(texto1+ " " +texto2)#Concatenación
print(texto1,texto2)
print(entero + 5)#Suma
print(decimal + 0.5)#Suma

#Cadena caso especial
texto3 = '"Hola"'#Para mostrar " " dentro, se abre con ' '
texto4 = "'Hola'"#Para mostrar ' ' dentro, se abre con " "
print(texto3,texto4)

#Convertir variables
texto5 = "5"
print(int(texto5) + 5)