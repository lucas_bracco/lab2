#DEFINICIÓN DE CLASES:
class mario_bross:
    #Self se refiere al objetivo mismo.
    #Para llamar a cada uno de los metodos o instancias se pasa primero por self
    #Contrucción o creación de objetos __init__
    #Destruccón de objetos __del__
    #INSTANCIACIÓN - Estado inicial del objeto
    def __init__ (self,color,letra,posicionX,posicionY):
        #Si queremos modificar el estado de un objeto o llamar un metodo propio
        #debemos usar self.atributo o self.metodo
        self.color = color
        self.letra = letra
        self.posicionX = posicionX
        self.posicionY = posicionY
        print("Se creo un Mario Bross con color:",color,"y letra:",letra)    
    #METODOS
    def subir(self):
        self.posicionY = self.posicionY + 1
    def bajar(self):
        self.posicionY = self.posicionY - 1
    def avanzar(self):
        self.posicionX = self.posicionX + 1
    def retroceder(self):
        self.posicionX = self.posicionX - 1
    def teletransporte(self,posicionX,posicionY):
        print("Bzzzz!!")
        self.posicionX = posicionX
        self.posicionY = posicionY

#Instanciar un objeto.
personaje1 = mario_bross("Rojo","M",0,0)
#Llamar a los metodos definidos en la clase.
personaje1.avanzar()
personaje1.avanzar()
personaje1.bajar()
personaje1.subir()
personaje1.retroceder()
print("Posición en X:",personaje1.posicionX,"Posición en Y:",personaje1.posicionY)
#Llamada de métodos que esperan parámetros.
personaje1.teletransporte(0,0)
print("Posición en X:",personaje1.posicionX,"Posición en Y:",personaje1.posicionY)

    