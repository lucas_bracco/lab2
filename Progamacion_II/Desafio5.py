#Generar de forma aleatoria un código con las siguientes características:
#los primeros 8 caracter debe ser letras.
#Debe ser una consonante seguida de una vocal.
#los 2 últimos caracter deben ser números.
#las letras y los números deben estar separados por ' _ '
import random


def generador ():
    palabra = ""
    for i in range(8):
        letra = chr(random.randint(ord('a'),ord('z')))
        if i % 2 == 0:
            while letra in ("a","e","i","o","u"):
                letra = chr(random.randint(ord('a'),ord('z')))
            palabra += letra
        if i % 2 == 1:
            while not (letra in ("a","e","i","o","u")):
                letra = chr(random.randint(ord('a'),ord('z')))
            palabra += letra
    palabra += "_"
    for i in range(2):
        palabra += str(random.randint(0,9))
    return palabra.title()


lista = []
for i in range(20):
    lista.extend([generador()])
for i in range(20):
    print(lista[i])
