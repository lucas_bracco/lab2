#Ciclo While

edad = 20

while edad > 15:
    edad -= 1
    print("La edad es:",edad)

#Ciclo For

lista1 = ["Uno","Dos","Tres"]

for i in lista1: #la variable i va tomando el valor de cada elemento de la lista hasta que finaliza.
    print(i)

for i in range(-4,4,1):#range(desde,hasta,incremento)
    print(i)

#For Recorrido inverso:

for i in reversed(lista1): #la variable i va tomando el valor de cada elemento de la lista hasta que finaliza.
    print(i)

for i in range(-4,4,-1):#range(desde,hasta,decremento)
    print(i)