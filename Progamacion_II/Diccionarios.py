#Definir un Diccionario
diccionario1 = {
    #Elemento / Valor
    "Perro" : "Chispita",
    "Gato" : "Curioso",
    "Tortuga" : "Veloz",
    #Se pueden crear listas dentro de diccionarios
    "Lista" : ["Esto","es","una","lista"],
    #Se pueden crear diccionarios dentro de diccionarios
    "diccionario2" : {
        "Activador1" : True,
        "Activador2" : False
    }
}

print(diccionario1["Perro"])
print(diccionario1["Gato"])
print(diccionario1["Tortuga"])
print(diccionario1["Lista"][0])
print(diccionario1["Lista"][1])
print(diccionario1["Lista"][2])
print(diccionario1["Lista"][3])
print(diccionario1["diccionario2"]["Activador1"])
print(diccionario1["diccionario2"]["Activador2"])