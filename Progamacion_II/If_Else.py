numero1 = 1
numero2 = 3

if numero1 < numero2 :
    print("El número mayor es:",numero2)
    if numero2 :
        print("Existe")
    else :
        print("No Existe")
else :
    print("El número mayor es:",numero1)
    if numero1 :
        print("Existe")
    else :
        print("No Existe")

condicion = True

if condicion: #Pregunta si condicion es True
    print("Verdadero")
else:
    print("Falso")

condicion = False

if not condicion: #Pregunta si condicion es False
    print("Falso")
else:
    print("Verdadero")