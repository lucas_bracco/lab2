#Ingresar por teclado las medidas de los lados de un triángulo e indicar tipo, perímetroy área.
# Área = (Base * Altura) / 2
# Perímetro = Lado1 + Lado2 + Lado3
#Equilatero = Todos sus lados iguales.
#Escaleno = Todos sus lados distintos.
#Isoseles = Dos lados iguales.

import math

lado1 = int(input("Ingrese primero valor: "))
lado2 = int(input("Ingrese segundo valor: "))
lado3 = int(input("Ingrese tercer valor: "))
m = (lado1 + lado2 + lado3) / 2
s = math.sqrt(m * (m - lado1) * (m - lado2) * (m - lado3))

if lado1 == lado2 == lado3 :
    print("El triángulo es Equilatero.")
else :
    if lado1 != lado2 and lado2 != lado3 and lado1 != lado3:
        print("El triángulo es Escaleno.")
    else :
        print("El triángulo es Isoseles.")
print("El perímetro del triángulo es:", lado1 + lado2 + lado3)
print("El área del triángulo es:", s)

