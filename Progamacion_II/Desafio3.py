#Generar un número aleatorio entre 1 y 10 indicando si el número que elegiste es menor, igual o adivinaste.
import random

num1 = random.randint(1,10)
op = 0
intentos = 1

while op != num1:
    op = int(input("Adivina el número secreto entre 1 y 10: "))
    if op < num1:
        print("El número que elegiste es menor.")
        intentos += 1
    elif op > num1:
        print("El número que elegiste es mayor.")
        intentos += 1
    else:
        print("Ganaste.")

print("Número secreto:",num1,"\nCantidad de intentos:",intentos)