#JUEGO DEL AHORCADO:
#Se debe elegir una palabra al azar del listado: ["Perro","Gato","Caballo","Oveja","Cerdo","Pato","Pez"]
#Se deben mostrar los respectivos guiones.
#Se debe ingresar una letra y mostrar o no si es correcta.
#El jugador tiene 3 vidas.
#"You Win" - "Game Over"

#Librerias
import random #Funciones aleatorias.
import os #system ("pause") ; system ("cls").

#FUNCIONES

#Función que verifica si la letra ingresada se encuentra en la palabra.
def verificador(a,b): #Parametros(Palabra elegida, Letra ingresada).
    aux = 0
    if b in a: #True si la letra se encuentra en la palabra.
        aux += 1
    return aux

#Función que va completando la palabra con las letras correctas.
def modificador(a,b,c,n): #Parametros(Palabra elegida, Letra ingresada,Palabra a completar,Tamaño de la Palabra).
    for i in range(1,n):
        if a[i] == b:
            c.pop(i) #Elimina el elemento (letra) del indice 'i'.
            c.insert(i,b) #Agrega nuevo elemento (letra) en el indice 'i'.
    return c #Retorna la lista auxiliar (palabra a completar).

#Función de validación.
def validar_letra(a,b):
    aux = 0
    if not a.isalpha(): #Si la letra ingresada no es alfabética.
        print("Debes ingresar una letra.")
        aux +=1
    if len(a) > 1: #Si se ingresa mas de una letra.
        print("No debes ingresar mas de una letra.")
        aux +=1
    if verificador(b,a) > 0: #Si se ingresa mas de una vez la misma letra.
        print("Ya usaste esa letra, elige otra nueva.")
        aux += 1
    return aux

#PROGRAMA PRINCIPAL

lista = ["Perro","Gato","Caballo","Oveja","Leon","Tigre","Zorro"]#Lista de palabras.
palabra = [""] #Lista auxiliar (palabra a completar).
usadas = "" #Cadena con las letras usadas.
numpalabra = random.randint(0,6) #Calcula un número aleatorio para elegir una palabra de la Lista.
tampalabra = len(lista[numpalabra]) #Calcula el tamaño de la palabra elegida. 
for i in range(tampalabra): #For para recorrer la palabra letra a letra.
    if i == 0:
        palabra[i] = lista[numpalabra][i] #Iguala la lista auxiliar con la primera letra de la palabra elegida.
    else:
        palabra.insert(i,"_") #El resto de la lista auxiliar se completa con "_".

vidas = 3 #Variable para las vidas del jugador.
victoria = False #Variable para la condición de victoria.

while vidas != 0 and victoria != True:
    #os.system("cls") #Limpia la consola.
    print("-----Juego del Ahorcado-----")
    #If Para dibujar el monigote dependiendo las vidas disponibles.
    if vidas == 3:
        print(" ╔════╗ vidas: ♥ ♥ ♥\n      ║\n      ║\n      ║\n      ╩",end=" ")
    elif vidas == 2:
        print(" ╔════╗ vidas: ♥ ♥ x\n Ô    ║\n      ║\n      ║\n      ╩",end=" ")
    elif vidas == 1:
        print(" ╔════╗ vidas: ♥ x x\n Ô    ║\n/|\   ║\n      ║\n      ╩",end=" ")

    for i in range(tampalabra):
        print(palabra[i],end=" ") #Muestra la palabra sin completar.
    print("")
    valid = 1 #Variable para repetir el while dependiendo el caracter ingresada por teclado.
    while valid != 0:
        letra = input("Elija una letra: ")
        #Llamada a la función
        valid = validar_letra(letra,usadas)
    usadas = usadas + letra #Agrega la Letra validada a la lista de Letras usadas.
    #Llamado a la Función.
    if verificador(lista[numpalabra],letra) == 0: #Determina si la letra ingresada no corresponde a la palabra.
        vidas -= 1 #Resta las vidas disponibles.
    else:
        #Llamado a la Función - Iguala la lista auxiliar (palabra sin completar)con el retorno de la función.
        palabra = modificador(lista[numpalabra],letra,palabra,tampalabra)
    if verificador(palabra,"_") == 0: #Verifica si ya no hay mas campos para completar "_".
        victoria = True

if vidas == 3 and victoria == True: #If para determinar si ganas o pierdes.
    print(" ╔════╗ vidas: ♥ ♥ ♥\n      ║\n      ║\n      ║\n      ╩",end=" ")
    print("Completaste la palabra:",end=" ")
    for i in range(tampalabra):
        print(palabra[i],end=" ")
    print("\nYou Win")
elif vidas == 2 and victoria == True: #If para determinar si ganas o pierdes.
    print(" ╔════╗ vidas: ♥ ♥ x\n Ô    ║\n      ║\n      ║\n      ╩",end=" ")
    print("Completaste la palabra:",end=" ")
    for i in range(tampalabra):
        print(palabra[i],end=" ")
    print("\nYou Win")
elif vidas == 1 and victoria == True: #If para determinar si ganas o pierdes.
    print(" ╔════╗ vidas: ♥ x x\n Ô    ║\n/|\   ║\n      ║\n      ╩",end=" ")
    print("Completaste la palabra:",end=" ")
    for i in range(tampalabra):
        print(palabra[i],end=" ")
    print("\nYou Win")
else:
    print(" ╔════╗ vidas: x x x\n Ô    ║\n/|\   ║\n/ \   ║\n      ╩",end=" ")
    print("No completaste la palabra.")
    print("Game Over")