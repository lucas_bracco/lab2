#Ingresar por teclado limite inferior y superior y hacer la sumatoria entre los numeros comprendidos.
rangoinf = int(input("Ingrese el rango inicial: "))
rangosup = int(input("Ingrese el rango final: "))

sumatoria = 0

for i in range(rangoinf,rangosup + 1):
    sumatoria += i

print("La sumatoria de los números entre (",rangoinf,"...",rangosup,") es:", sumatoria)