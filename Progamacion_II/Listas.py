#Definir una Listas
lista = ["Nombre",1,["Hola","Mundo"],[2,3]]#Se pueden crear listas dentro de listas
print(lista)
#Elemento 1 de la lista
print(lista[0])
#Elemento 2 de la lista
print(lista[1])
#Elemento 1 del Elemento 3 de la lista
print(lista[2][0])
#Elemento 2 del Elemento 3 de la lista
print(lista[2][1])
#Elemento 1 del Elemento 4 de la lista
print(lista[3][0])
#Elemento 2 del Elemento 4 de la lista
print(lista[3][1])