class peon:
    def __init__(self,posicionX,posicionY,color):
        self.posicionX = posicionX
        self.posicionY = posicionY
        self.color = color
        self.viva = True
    def __del__(self):
        print("La pieza se fue.")
    def comer(self):
        if (self.color == "negro"):
            self.posicionY = self.posicionY + 1
            self.posicionX = self.posicionX + 1
        else:
            self.posicionY = self.posicionY - 1
            self.posicionX = self.posicionX - 1
    def avanzar(self):
        if (self.color == "negro"):
            self.posicionY = self.posicionY + 1
        else:
            self.posicionY = self.posicionY - 1

#PRINCIPAL
pieza1 = peon(1,1,"negro")
pieza2 = peon(2,2,"blanco")

pieza1.avanzar()
pieza1.comer()
print(pieza1.posicionX,pieza1.posicionY)

pieza2.avanzar()
pieza2.comer()
print(pieza2.posicionX,pieza2.posicionY)

del pieza1
del pieza2
