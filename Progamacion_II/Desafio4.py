#Encriptar y Desencriptar palabras teniendo en cuenta:
#{M=0;U=1;R=2;C=3;i=4;E=5;L=6;A=7;G=8;O=9}

#FUNCION
def codificador(a,b,c):
    for i in range(len(a)):
        for j in range(len(b)):
            if a[i] == b[j]:
                a = a.replace(a[i],c[j])
    return a
lista1 = ["M","U","R","C","I","E","L","A","G","O"]
lista2 = ["0","1","2","3","4","5","6","7","8","9"]

print("1.Encriptar\n2.Desencriptar")
op = int(input("Seleccione una opción: "))
if op == 1:
    palabra = input("Escriba la palabra a encriptar: ").upper()
    palabra = codificador(palabra,lista1,lista2)
    print("Palabra encriptada:",palabra)

elif op == 2:
    palabra = input("Escriba el código para desencriptar: ").upper()
    palabra = codificador(palabra,lista2,lista1)
    print("Código desencriptado:",palabra)