#Realizar un programa que permita cargar en una lista palabras hasta que se ingrese una palabra vacía.
#Mostrar las palabras ordenadas.

lista = []
#Ejemplo de un Do while, hay que usar un break para salir del bucle.
while True:
    lista.append(input("Ingrese una palabra: "))
    if len(lista[-1]) == 0:
        lista.pop(-1)
        break
lista.sort()
for item in lista:
    print(item)
    
