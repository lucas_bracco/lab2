#Modulo circulo.
#Diametro = 2 * (radio).
#Perímetro = 2 * pi * (radio).
#Área = pi * (radio ** 2).

from math import pi

def diametro (r):
    d = r * 2
    return d

def perimetro (r):
    p = 2 * pi * r
    return p

def area (r):
    a = pi * (r ** 2)
    return a
