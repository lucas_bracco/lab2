#Se debe pedir radio y calcular:
#Diametro, Perímetro y el Área.

from circulo import diametro, perimetro, area

radio = float(input("Ingrese el radio de un circulo: "))

print("El Diámetro es:", round(diametro(radio),2))
print("El Perímetro es:",round(perimetro(radio),2))
print("El Área es:",round(area(radio),2))