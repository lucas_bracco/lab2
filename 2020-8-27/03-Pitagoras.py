#Crear una Función para calcular Pitagoras.

#Importa de la librería solo la función requerida.
from math import sqrt

def pitagoras (a,b):
    c = sqrt(a ** 2 + b ** 2)
    return c

lado1 = float(input("Ingrese la altura: "))
lado2 = float(input("Ingrese la base: "))

print("H =",pitagoras(lado1,lado2))