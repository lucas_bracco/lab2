lista_centigrado = [0.0,10.0,20.0,30.0,40.0,50.0,60.0,70.0,80.0,90.0]

def convertir(num):
    return num * 9/5 + 32

lista_farenheit = list(map(convertir,lista_centigrado))

print("°C",lista_centigrado,"\n°F",lista_farenheit)
