from math import pi

class Cilindro:

    def __init__(self, radio, altura):
        self.radio = radio
        self.altura = altura
    
    def area(self):
        return 2 * pi * self.radio * (self.radio + self.altura)

    def volumen(self):
        return pi * (self.radio ** 2) * self.altura
    
cilindro1 = Cilindro(2,8)

print("Area = ",round(cilindro1.area(),2))
print("Volumen = ",round(cilindro1.volumen(),2))