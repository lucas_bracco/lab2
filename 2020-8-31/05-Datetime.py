#Crear datetime a partir de formato.

from datetime import datetime, timedelta

fecha1 = "2020|08|31"
fecha2 = "2020|12|24"

dt1 = datetime.strptime(fecha1, "%Y|%m|%d")
dt2 = datetime.strptime(fecha2, "%Y|%m|%d")

resta = dt2 - dt1
print("Para Navidad faltan",resta)

td = timedelta(days=5)
print("Dentro de 5 días es", (dt1 + td).strftime("%x"))