#Maneras de dar formato a la fecha y hora.

from datetime import datetime

ahora = datetime.now()

print("ahora =", ahora)
print("Fecha:", ahora.strftime("%d/%m/%Y"))
print("Fecha:", ahora.strftime("%x"))

print("Hora", ahora.strftime("%H:%M:%S"))
print("Hora", ahora.strftime("%X"))