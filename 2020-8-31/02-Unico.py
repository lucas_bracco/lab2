#Cargar una Lista de números enteros.
#Implementar una funcion que devuelva una nueva Lista con los elementos únicos de la anterior.
#Ej: 
#Entrada = [1, 2, 3, 3, 3, 4, 4, 6, 8]
#Salida = [1, 2, 3, 4, 6 ,8]

def lista_unica(param):
    aux = []
    for item in param:
        if not (item in aux):
            aux.append(item)
    return aux

lista1 = []
while True:
    lista1.append(input("Ingrese un número: "))
    if len(lista1[-1]) == 0:
        lista1.pop(-1)
        break
    
    lista2 = lista_unica(lista1)

for item in lista2:
    print(item,end=" ")