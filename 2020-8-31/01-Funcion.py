#Funcion que devuelve 3 resultados.
def multiret (param):
    return 2, param * 3, param

#Se los guarda de la siguiente forma:
a, b, c = multiret(10)

print(a, b, c)

#Otra forma sería:
d = multiret(10)
print(d)