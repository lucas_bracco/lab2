from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5 import uic

class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("2020-10-05/03-boton.ui",self)
        self.boton1.clicked.connect(self.on_clicked1)
        self.boton2.clicked.connect(self.on_clicked2)

    def on_clicked1(self):
        self.boton2.setEnabled(True)
        self.boton1.setEnabled(False)
    def on_clicked2(self):
        self.boton1.setEnabled(True)
        self.boton2.setEnabled(False)

app = QApplication([])

win = MiVentana()
win.show()

app.exec_()