from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5 import uic

class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("2020-10-05/02-texto.ui",self)
        self.boton.clicked.connect(self.on_clicked)

    def on_clicked(self):
        self.etiqueta.setText(self.texto.text())

app = QApplication([])

win = MiVentana()
win.show()

app.exec_()