import sqlite3
import bcrypt

#Conectar con la base de datos.
conexion = sqlite3.connect("2020-11-26/Cuentas.db")

nomb = input("Ingrese usuario: ")
passw = input("Ingrese contraseña: ")
salt = bcrypt.gensalt()
claveHash = bcrypt.hashpw(passw.encode("utf-8"),salt)

cursor = conexion.cursor()
cursor.execute("INSERT INTO Usuarios(User,Password) VALUES(?,?)",(nomb,claveHash.decode("utf-8")))

conexion.commit()
conexion.close()