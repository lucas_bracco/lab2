#Funciones Hash
import bcrypt

#--------String Bytes-------------
clave1 = b'123456' 
clave2 = '123456'.encode("utf-8")
#---------------------------------

salt = bcrypt.gensalt()
claveHash = bcrypt.hashpw(clave2,salt)

if bcrypt.checkpw(clave1,claveHash):
    print("La contraseña coincide")
else:
    print("La contraseña no coincide")