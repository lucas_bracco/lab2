from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic


class MiVentana(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("2020-10-19/02-lista.ui", self)
        self.lista.itemSelectionChanged.connect(self.on_item_changed)

    def on_item_changed(self):
        self.seleccion.clear()
        items = self.lista.selectedItems()
        for item in items:
            self.seleccion.addItem(item.text())


app = QApplication([])

win = MiVentana()
win.show()

app.exec_()
