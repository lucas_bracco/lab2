# Pedir un mes al usuario (Enero ... Diciembre)
# Mostrar a que estación pertenece (Primavera, Verano, Otoño, Invierno)


mes = input("Elija un mes: ").title()

if mes in ("Septiembre", "Octubre", "Noviembre"):
    print("La estación es Primavera.")
elif mes in ("Diciembre", "Enero", "Febrero"):
    print("La estación es Verano.")
elif mes in ("Marzo", "Abril", "Mayo"):
    print("La estación es Otoño.")
elif mes in ("Junio", "Julio", "Agosto"):
    print("La estación es Invierno.")
else:
    print("Ingrese un mes existente.")