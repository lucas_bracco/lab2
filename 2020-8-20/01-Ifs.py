#Pedir al usuario un tipo de Temperatura (Celsius o Farenhel)
#Pedir la Temperatura.
#Cambiar a la otra Temperatura.

# C = (5 * (F - 32)) / 9
# F = (9 * C + 32 * 5)/5

Op = 0
temp = 0
tempaux = 0

while Op == 0:
    print("1-Celsius.\n2-Farenhel.")
    Op = int(input("Indique con que tipo de temperatura va a trabajar: "))
    temp = float(input("Ingrese la cantidad de temperatura: "))
    if Op == 1:
        print("Temperatura en Celsius:",temp,"°C")
        tempaux = (9 * temp + 32 * 5)/5
        print("Temperatura en Farenhel:",tempaux,"°F")
    elif Op == 2:
        print("Temperatura en Farenhel:",temp,"°F")
        tempaux = (5 * (temp - 32)) / 9
        print("Temperatura en Celsius:",tempaux,"°C")
    else:
        print("Opción ingresada no es correcta.")
        Op = 0